Program Created: 
July 17th, 2017

Created By:
Jacky Li

Program Used By: 
Maintenance

Program's Purpose: 
Used to hide some of the schemes on the PASAN computers so that techs and maintenance do not get confused on which scheme to pick when flash testing panels.

Deployed:
Roy Lu has a copy of this program.

Referneces:
PASAN Data Collection


