﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Xml;
using System.Windows.Forms;

namespace HideScheme
{
    public partial class SchemeHide : Form
    {
        public SchemeHide()
        {
            InitializeComponent();
            Listselection();


        }




        private void Listselection()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            ListdataVisible();
            ListdataHidden();
        }
        private void ListdataVisible()
        {
            string sql = @"select [SchemeNM],interID from [Scheme] where InterID not in (select distinct schemeinterid from SchemeHidden where SchemeStatus = 0)";
            DataTable dt = PullData(sql);
            foreach(DataRow row in dt.Rows)
            {
                listBox1.Items.Add(row[0].ToString());
            }
        }
        private void ListdataHidden()
        {
            string sql = @"select distinct SchemeNM from SchemeHidden where SchemeStatus = 0";
            DataTable dt = PullData(sql);
            foreach (DataRow row in dt.Rows)
            {
                listBox2.Items.Add(row[0].ToString());
            }
        }


        public static DataTable PullData(string query, string connString = "")
        {
            if (string.IsNullOrEmpty(connString))
                connString = @"Data Source = 10.127.34.15; Initial Catalog = LabelPrintDB; User ID = fastengine; Password = Csi456";
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(query, conn);

            try
            {
                conn.Open();
                // create data adapter
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                da.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                if (conn != null && conn.State != ConnectionState.Closed)
                    conn.Close();
            }

        }
        public static int PutsData(string query, string connString = "")
        {
            if (string.IsNullOrEmpty(connString))
                connString = @"Data Source = 10.127.34.15; Initial Catalog = LabelPrintDB; User ID = fastengine; Password = Csi456";
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();
            int i = 0;
            try
            {

                SqlCommand myCommand = new SqlCommand(query, conn);

                i = myCommand.ExecuteNonQuery();
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                if (conn != null && conn.State != ConnectionState.Closed)
                    conn.Close();

            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void bTHide_Click(object sender, EventArgs e)
        {
            foreach (string scheme in listBox1.SelectedItems)
            {
                string sql = "insert into [SchemeHidden] values( (select top 1 interid from Scheme where schemeNM = '"+ scheme + "'), '" + scheme + "','0','Jacky')";
                PutsData(sql);
            }
            Listselection();
        }

        private void bTShow_Click(object sender, EventArgs e)
        {
            foreach (string scheme in listBox2.SelectedItems)
            {
                string sql = "delete from [SchemeHidden] where SchemeNM = '" + scheme + "'" ;
                PutsData(sql);
            }
            Listselection();
        }
    }
}
